About
=====

This is a peer grading based group assignment activity for Moodle 

This module enables a group of students submit one file as their
assignment. In addition to the file submission the students can rate 
their fellow group members activity on the project.
The activity then calculates the grade the students receive.
The teacher still has to apply the grade to the submission
The final grade is based on the teacher grade and the group grade. 


Authors
=======

2013
 - This module was built to a specification from City University London 
 - by Learning Technology Services Ltd, Ireland.

2017
 - This module was revised by Naomi Wilce, City University London


Features
========

 - students can grade their group members effort on a group assignment
 - teachers can grade the group assignment submission
 - the final grade is calculated based on both grades
 - each student gets an individual final grade


 - teachers can choose between two calculations used to determine the final grade
 - simple creates individual grades based on the students peer marks and the assignment grade
 - outlier option has a more complicated formula which uses standard deviation to remove any
 large inconsistencies between peer grades from the calculation


Supported Versions
==================

 - The module has been tested with Moodle versions 2.4, 2.5, 2.6, 3.2, 3.4 


Installation instructions
=========================

1. Copy the peerassessment directory to the mod directory of the Moodle instance
2. Log into your Moodle as administrator, or if logged in visit the Notifications 
   page
3. You should be prompted to upgrade your Moodle to install the module
4. Once installed there are no global settings for this activity.

Usage instructions
==================

1. The Peer Assessment module will now appear in the Add an activity or resource dialogue box

For more information, visit the following Moodle Docs Webpage
http://docs.moodle.org/en/Installing_contributed_modules_or_plugins
